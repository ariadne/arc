CURDIR = .
OUTDIR = build

arc = $(wildcard ./arcs/arc-*.xml)
arc_xmls = $(basename $(notdir $(arc)))
arc_htmls = $(addsuffix .html, $(addprefix $(OUTDIR)/, $(arc_xmls)))
index_html = $(OUTDIR)/index.html
other_htmls = $(OUTDIR)/about.html
build_deps = $(addprefix $(OUTDIR)/deps/, $(notdir $(wildcard arcs/deps/*)))

.PHONY: main
main: $(build_deps) $(index_html) $(other_htmls) $(arc_htmls)

.PHONY: clean
clean:
	@rm -rf $(OUTDIR)
	@echo "Directory cleaned"

$(index_html): $(OUTDIR)/%.html: arcs/%.xml | $(OUTDIR)
	@xsltproc --output $@ ./arcs/deps/index.xsl $< && echo "Finished building $@"
	@sed -i 's/index.xml/index.html/' $@
	@sed -i 's/about.xml/about.html/' $@
	@sed -i -E 's/arc-([0-9]{4}).xml/arc-\1.html/' $@

$(other_htmls): $(OUTDIR)/%.html: arcs/%.xml | $(OUTDIR)
	@xsltproc --output $@ ./arcs/deps/page.xsl $< && echo "Finished building $@"
	@sed -i 's/index.xml/index.html/' $@
	@sed -i 's/about.xml/about.html/' $@
	@sed -i -E 's/arc-([0-9]{4}).xml/arc-\1.html/' $@

$(arc_htmls): $(OUTDIR)/arc-%.html: arcs/arc-%.xml | $(OUTDIR)
	@xsltproc --output $@ ./arcs/deps/arc.xsl $< && echo "Finished building $@"
	@sed -i 's/index.xml/index.html/' $@
	@sed -i 's/about.xml/about.html/' $@
	@sed -i -E 's/arc-([0-9]{4}).xml/arc-\1.html/' $@

$(build_deps): build/deps/%: arcs/deps/% | $(OUTDIR)
	@mkdir -p build/deps/ && cp $< $@ && echo "Finished copying $@"

$(OUTDIR):
	@mkdir -p "$@"