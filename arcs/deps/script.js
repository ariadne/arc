document.querySelectorAll(".md").forEach(e => {
    const raw = e.innerText;
    const converter = new showdown.Converter();
    converter.setOption('headerLevelStart', 3);
    const proc = converter.makeHtml(raw);
    e.outerHTML = proc;
});

hljs.highlightAll();