<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:arc="https://ariadne.id/ns/arc">

<xsl:template match="/">
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>ARC-<xsl:value-of select="/arc:arc/arc:header/arc:number"/>: <xsl:value-of select="/arc:arc/arc:header/arc:title"/></title>
        <link rel="stylesheet" href="deps/style.css" />
    </head>
    <body>
        <div class="container">
            <xsl:apply-templates select="arc:arc"/>
        </div>
    </body>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.3.1/build/styles/mono-blue.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.3.1/build/highlight.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/showdown@1.9.1/dist/showdown.min.js"></script>
    <script src="deps/script.js"></script>
</html>
</xsl:template>

<xsl:template match="arc:arc">
    <h1>ARC-<xsl:value-of select="arc:header/arc:number"/>: <xsl:value-of select="arc:header/arc:title"/></h1>
    <p><a href="index.xml">Back to index</a></p>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Type</p></th>
            <td><xsl:value-of select="arc:header/arc:type"/></td>
        </tr>
        <tr>
            <th><p>Abstract</p></th>
            <td><xsl:value-of select="arc:header/arc:abstract"/></td>
        </tr>
        <tr>
            <th><p>Authors</p></th>
            <td>
                <p>
                    <xsl:for-each select="arc:header/arc:author">
                        <xsl:value-of select="arc:name"/>
                        <xsl:if test="arc:email">
                            &lt;<a>
                                <xsl:attribute name="href">mailto:<xsl:value-of select="arc:email"/></xsl:attribute>
                                <xsl:value-of select="arc:email"/>
                            </a>&gt;
                        </xsl:if> 
                        <br/>
                    </xsl:for-each>
                </p>
            </td>
        </tr>
        <tr>
            <th><p>Status</p></th>
            <td><xsl:value-of select="arc:header/arc:status"/></td>
        </tr>
        <tr>
            <th><p>Creation date</p></th>
            <td><xsl:value-of select="arc:header/arc:date"/></td>
        </tr>
        <xsl:if test="arc:header/arc:discussion != ''">
            <tr>
                <th><p>Discussion</p></th>
                <td>
                    <a>
                        <xsl:attribute name="href"><xsl:value-of select="arc:header/arc:discussion"/></xsl:attribute>
                        <xsl:value-of select="arc:header/arc:discussion"/>
                    </a>
                </td>
            </tr>
        </xsl:if> 
        <xsl:if test="arc:header/arc:mailinglist != ''">
            <tr>
                <th><p>Mailing list</p></th>
                <td>
                    <a>
                        <xsl:attribute name="href"><xsl:value-of select="arc:header/arc:mailinglist"/></xsl:attribute>
                        <xsl:value-of select="arc:header/arc:mailinglist"/>
                    </a>
                </td>
            </tr>
        </xsl:if> 
    </table>

    <h2>Description</h2>
    <pre class="md">
        <xsl:value-of select="arc:description"/>
    </pre>

    <xsl:if test="arc:header/arc:type = 'Standards Track'">
        <xsl:if test="arc:header/arc:category = 'Cryptographic Material Discovery Protocol'">
            <xsl:apply-templates select="arc:definition/arc:cryptographicMaterialDiscoveryProtocol" mode="cmdp"/>
        </xsl:if> 
        <xsl:if test="arc:header/arc:category = 'Cryptographic Standard Protocol'">
            <xsl:apply-templates select="arc:definition/arc:proofRequestProtocol" mode="csp"/>
        </xsl:if> 
        <xsl:if test="arc:header/arc:category = 'Proof Request Protocol'">
            <xsl:apply-templates select="arc:definition/arc:proofRequestProtocol" mode="prp"/>
        </xsl:if> 
        <xsl:if test="arc:header/arc:category = 'Service Provider Definition'">
            <xsl:apply-templates select="arc:definition/arc:serviceProviderDefinition" mode="spd"/>
        </xsl:if> 
    </xsl:if> 
</xsl:template>

<xsl:template match="arc:definition/arc:cryptographicMaterialDiscoveryProtocol" mode="cmdp">
    <xsl:call-template name="arc:about">
        <xsl:with-param name="nodes" select="../arc:about" />
    </xsl:call-template>
</xsl:template>

<xsl:template match="arc:definition/arc:cryptographicStandardProtocol" mode="csp">
    <xsl:call-template name="arc:about">
        <xsl:with-param name="nodes" select="../arc:about" />
    </xsl:call-template>
</xsl:template>

<xsl:template match="arc:definition/arc:proofRequestProtocol" mode="prp">
    <xsl:call-template name="arc:about">
        <xsl:with-param name="nodes" select="../arc:about" />
    </xsl:call-template>
    
    <h2>Parameters</h2>

    <table cellspacing="0" cellpadding="0">
        <tr>
            <th><p>Name</p></th>
            <th><p>Type</p></th>
            <th><p>Mandatory</p></th>
            <th><p>Default value</p></th>
        </tr>
        <xsl:for-each select="/arc:arc/arc:definition/arc:proofRequestProtocol/arc:parameter">
            <xsl:call-template name="arc:parameter">
                <xsl:with-param name="nodes" select="../arc:parameter" />
            </xsl:call-template>
        </xsl:for-each>
    </table>

    <h2>Possible returns</h2>

    <table cellspacing="0" cellpadding="0">
        <tr>
            <th><p>Format</p></th>
        </tr>
        <xsl:for-each select="/arc:arc/arc:definition/arc:proofRequestProtocol/arc:return">
            <xsl:call-template name="arc:return">
                <xsl:with-param name="nodes" select="../arc:return" />
            </xsl:call-template>
        </xsl:for-each>
    </table>
</xsl:template>

<xsl:template match="arc:definition/arc:serviceProviderDefinition" mode="spd">
    <xsl:call-template name="arc:about">
        <xsl:with-param name="nodes" select="../arc:about" />
    </xsl:call-template>

    <h2>Profile</h2>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Username</p></th>
            <td><code><xsl:value-of select="arc:profile/arc:username"/></code></td>
        </tr>
        <tr>
            <th><p>URI</p></th>
            <td><code><xsl:value-of select="arc:profile/arc:uri"/></code></td>
        </tr>
        <tr>
            <th><p>QR</p></th>
            <td><code><xsl:value-of select="arc:profile/arc:qr"/></code></td>
        </tr>
    </table>

    <h2>Claim</h2>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>URI (regex)</p></th>
            <td><code><xsl:value-of select="arc:claim/arc:uri"/></code></td>
        </tr>
        <tr>
            <th><p>URI is ambiguous?</p></th>
            <td><xsl:value-of select="arc:claim/arc:ambiguous"/></td>
        </tr>
    </table>

    <h2>Proof</h2>
    <h3>Request</h3>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Protocol</p></th>
            <td>
                <a>
                    <xsl:attribute name="href">arc-<xsl:value-of select="arc:proof/arc:request/arc:protocol/arc:arc"/>.xml</xsl:attribute>
                    <xsl:value-of select="arc:proof/arc:request/arc:protocol/arc:name"/> (ARC-<xsl:value-of select="arc:proof/arc:request/arc:protocol/arc:arc"/>)
                </a>
            </td>
        </tr>
        <tr>
            <th><p>Parameters</p></th>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                    </tr>
                    <xsl:for-each select="arc:proof/arc:request/arc:parameter">
                        <tr>
                            <td><xsl:value-of select="arc:name"/></td>
                            <td><code><xsl:value-of select="arc:value"/></code></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>

    <h3>Response</h3>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Format</p></th>
            <td><xsl:value-of select="arc:proof/arc:response/arc:format"/></td>
        </tr>
    </table>

    <h3>Target</h3>
    <table cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Format</p></th>
            <td><xsl:value-of select="arc:proof/arc:target/arc:format"/></td>
        </tr>
        <tr>
            <th><p>Relation</p></th>
            <td><xsl:value-of select="arc:proof/arc:target/arc:relation"/></td>
        </tr>
        <tr>
            <th><p>Path</p></th>
            <td><code><xsl:value-of select="arc:proof/arc:target/arc:path"/></code></td>
        </tr>
    </table>
</xsl:template>

<xsl:template name="arc:about">
    <h2>About</h2>
    
    <table  cellspacing="0" cellpadding="0" class="table--horizontal">
        <tr>
            <th><p>Name</p></th>
            <td><p><xsl:value-of select="arc:about/arc:name"/></p></td>
        </tr>
        <tr>
            <th><p>Short name</p></th>
            <td><p><xsl:value-of select="arc:about/arc:shortname"/></p></td>
        </tr>
        <tr>
            <th><p>URL</p></th>
            <td>
                <p>
                    <a>
                        <xsl:attribute name="href"><xsl:value-of select="arc:about/arc:url"/></xsl:attribute>
                        <xsl:value-of select="arc:about/arc:url"/>
                    </a>
                </p>
            </td>
        </tr>
    </table>
</xsl:template>

<xsl:template name="arc:parameter">
    <tr>
        <td><p><xsl:value-of select="arc:name"/></p></td>
        <td><p><xsl:value-of select="arc:type"/></p></td>
        <td><p><xsl:value-of select="arc:mandatory"/></p></td>
        <td><p><xsl:value-of select="arc:defaultValue"/></p></td>
    </tr>
</xsl:template>

<xsl:template name="arc:return">
    <tr>
        <td><p><xsl:value-of select="arc:format"/></p></td>
    </tr>
</xsl:template>

</xsl:stylesheet>