<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>ARC</title>
        <link rel="stylesheet" href="deps/style.css" />
    </head>
    <body>
        <div class="container">
            <h1><xsl:value-of select="page/title"/></h1>
            <p><a href="index.xml">Back to index</a></p>
            <pre class="md">
                <xsl:value-of select="page/content"/>
            </pre>
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/showdown@1.9.1/dist/showdown.min.js"></script>
    <script src="deps/script.js"></script>
</html>
</xsl:template>

</xsl:stylesheet>