<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:arc-short="https://ariadne.id/ns/arc-short">

<xsl:template match="/">
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>ARC</title>
        <link rel="stylesheet" href="deps/style.css" />
    </head>
    <body>
        <div class="container">
            <h1>Ariadne Spec</h1>
            <p>A living document on cryptographic decentralized identity verification.</p>
            <h2>Links</h2>
            <p>
                <ul>
                    <li>
                        <a href="/about.xml">About the Ariadne Spec</a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="https://codeberg.org/ariadne/arc">Source repository</a><span class="link-domain">(codeberg.org)</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="https://codeberg.org/ariadne/arc-voting">Voting repository</a><span class="link-domain">(codeberg.org)</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="https://lists.sr.ht/~yarmo/arc-discuss">arc-discuss mailing list</a><span class="link-domain">(sr.ht)</span>
                    </li>
                </ul>
            </p>
            <h2>ARCs</h2>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <th><p>Number</p></th>
                    <th><p>Name</p></th>
                    <th><p>Type</p></th>
                    <th><p>Status</p></th>
                    <th><p>Date</p></th>
                </tr>
                <xsl:apply-templates select="/arcs/arc-short:arc-short">
                    <xsl:sort select="arc-short:number"/>
                </xsl:apply-templates>
            </table>
        </div>
    </body>
</html>
</xsl:template>

<xsl:template match="arc-short:arc-short">
    <tr>
        <td>
            <p><a><xsl:attribute name="href">arc-<xsl:value-of select="arc-short:number"/>.xml</xsl:attribute>ARC-<xsl:value-of select="arc-short:number"/></a></p>
        </td>
        <td>
            <p><xsl:value-of select="arc-short:title"/></p>
        </td>
        <td>
            <p><xsl:value-of select="arc-short:type"/></p>
        </td>
        <td>
            <p><xsl:value-of select="arc-short:status"/></p>
        </td>
        <td>
            <p><xsl:value-of select="arc-short:date"/></p>
        </td>
    </tr>
</xsl:template>

</xsl:stylesheet>