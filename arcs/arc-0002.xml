<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?xml-stylesheet type="text/xsl" href="deps/arc.xsl"?>
<arc xmlns="https://ariadne.id/ns/arc">
    <header>
        <title>Ariadne Spec governance</title>
        <abstract>
            <p>This document describes and keeps track of the Ariadne Spec governance model.</p>
        </abstract>
        <author>
            <name>Yarmo Mackenbach</name>
            <email>yarmo@keyoxide.org</email>
            <openpgp4fpr>9f0048ac0b23301e1f77e994909f6bd6f80f485d</openpgp4fpr>
        </author>
        <date>2021-10-11</date>
        <number>0002</number>
        <status>Active</status>
        <type>Informational</type>
        <category/>
        <supersedes/>
        <supersededby/>
        <discussion/>
        <mailinglist/>
        <license>
            <shorthand>CC0-1.0</shorthand>
            <url>https://spdx.org/licenses/CC0-1.0.html</url>
        </license>
        <revision>
            <version>1.0</version>
            <date>2021-10-11</date>
            <remark>Initial version.</remark>
        </revision>
    </header>
    <description>
# Introduction

The Ariadne Spec governance model follows the community governance model, in which a core team takes on the responsibility of decision making. A Steering Council exists to prevent the Ariadne Spec from straying from its Objectives but ideally never exercises its veto power.

# Groups and their functions

## The ARC Core Team

Members of the ARC Core Team have the mission to allow the Ariadne Spec to best fulfill its Objectives as stated in [ARC-0001](arc-0001.xml).

They pursue their mission by making any of the following contributions:

- submitting ARCs;
- reviewing and commenting on ARCs;
- voting on the progress of ARCs;
- responding to questions on the mailing lists or the issue tracker;
- triaging issues on the issue tracker;
- moderating discussions;
- advocating for secure decentralized identity solutions in general.

Anyone can become member of the ARC Core Team, regardless of their skills. Candidates must be nominated by existing ARC Core Team members and will be voted in by the other members.

To maintain a healthy member composition free of external influence, no more than 1/3rd of the members may work for or be directly involved in any other capacity with the same company or organization. A person cannot be a candidate if its membership would cause this 1/3rd threshold to be crossed.

ARC Core Team members are not required to vote and may be dormant at their leisure, all contributions are exclusively on a voluntary basis. It is expected of ARC Core Team members to revoke their membership when they no longer wish to participate.

ARC Core Team members—both active and dormant—may also be removed from the ARC Core Team by vote (see **Revoking an ARC Core Team membership** below).

## The ARC Editor

One ARC Core Team member will also assume the role of ARC Editor, who has the added responsibility of performing the repository actions—merging Pull Requests, changing an ARC's status, etc.—and organizing votes. The ARC Editor will have increased permissions in the ARC repository.

In case the ARC Editor can no longer take care of their responsabilities, the ARC Steering Council will appoint one of the ARC Core Members as the new ARC Editor.

## The ARC Steering Council

The ARC Steering Council has the mission to supervise the activities of the ARC Core Team and prevent the Ariadne Spec from straying from its Objectives. It can issue a veto on any vote performed by the ARC Core Team. This ARC also tracks every instance of the veto power being exercised to ensure governance transparency.

Ideally, the veto power is never exercised, and all ARC Steering Council members are well aware of this.

Members of the ARC Steering Council will also have administrative access to key resources—code repository, web hosting, etc.—to increase the bus factor.

The ARC Steering Council will decide on its member composition in internal discussions, due to the high level of resource access granted to its members. 

# Voting

The voting process is described in [ARC-0003](arc-0003.xml).

## ARC Approval

ARC Core Team members must vote on a new or updated ARC during the Proposal stage of the ARC's lifetime. During the 14-day voting period, their choices are to either:

- accept the ARC;
- reject the ARC;
- revert the ARC to the Draft status.

The vote is decided with a plurality of votes.

If the ARC is an update to an existing ARC and is rejected, the original ARC will remain active.

## ARC Deprecation

If a significant number of ARC Core Team members ask for the deprecation of an Active ARC, a 14-day voting period will be initiated. The choices for the ARC Core Team members are to either:

- keep the ARC active;
- deprecate the ARC.

The vote is decided with a majority of votes.

## Nominating new ARC Core Team members

Nominations of new ARC Core Team members are voted on in ARC Core Team members-only issues on the repository issue tracker. The choices are either:

- in favor of the nomination;
- against the nomination.

The nomination requires a supermajority of two-thirds of votes.

## Revoking an ARC Core Team membership

For any reason, the current membership of an ARC Core Team member may be called to vote upon. The choices are to either:

- keep the membership;
- revoke the membership.

The revokation requires a supermajority of two-thirds of votes.

# Current ARC Core Team members

- Wiktor Kwapisiewicz &lt;[wiktor@metacode.biz](mailto:wiktor@metacode.biz)&gt; [openpgp4fpr:653909A2F0E37C106F5FAF546C8857E0D8E8F074](https://keyoxide.org/653909A2F0E37C106F5FAF546C8857E0D8E8F074)

- Yarmo Mackenbach &lt;[yarmo@keyoxide.org](mailto:yarmo@keyoxide.org)&gt; [openpgp4fpr:9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d)

# Current ARC Editor

- Yarmo Mackenbach &lt;[yarmo@keyoxide.org](mailto:yarmo@keyoxide.org)&gt; [openpgp4fpr:9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d)

# Current ARC Steering Council members

- Wiktor Kwapisiewicz &lt;[wiktor@metacode.biz](mailto:wiktor@metacode.biz)&gt; [openpgp4fpr:653909A2F0E37C106F5FAF546C8857E0D8E8F074](https://keyoxide.org/653909A2F0E37C106F5FAF546C8857E0D8E8F074)

- Yarmo Mackenbach &lt;[yarmo@keyoxide.org](mailto:yarmo@keyoxide.org)&gt; [openpgp4fpr:9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d)

# Veto history

The ARC Steering Council has not issued a veto yet.
    </description>
    <definition/>
</arc>